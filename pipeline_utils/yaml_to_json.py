"""
Author: @ponchotitlan
This script converts a YAML file into a JSON file. The script receives the YAML file path
as a command-line argument and outputs the converted JSON to a file with the same base name.
"""

import yaml
import json
import sys
import os

def convert_yaml_to_json(yaml_file_path, json_file_path):
    """
    Converts a YAML file to a JSON file.

    Args:
        yaml_file_path (str): The path to the input YAML file.
        json_file_path (str): The path to the output JSON file.
    """
    try:
        with open(yaml_file_path, 'r') as yaml_file:
            data = yaml.safe_load(yaml_file)
        
        with open(json_file_path, 'w') as json_file:
            json.dump(data, json_file, indent=4)
        
        print(f"Successfully converted {yaml_file_path} to {json_file_path}")
    except Exception as e:
        print(f"Error: {e}")
        sys.exit(1)

if __name__ == "__main__":
    """
    Command-line interface for converting a YAML file to a JSON file.

    Usage:
        python convert_yaml_to_json.py <path_to_yaml_file>

    The output JSON file will have the same base name as the input YAML file.
    """
    if len(sys.argv) != 2:
        print("Usage: python convert_yaml_to_json.py <path_to_yaml_file>")
        sys.exit(1)
    
    yaml_file_path = sys.argv[1]
    if not os.path.isfile(yaml_file_path):
        print(f"Error: The file {yaml_file_path} does not exist.")
        sys.exit(1)
    
    json_file_path = os.path.splitext(yaml_file_path)[0] + '.json'
    convert_yaml_to_json(yaml_file_path, json_file_path)