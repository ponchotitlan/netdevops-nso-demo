"""
Author: @ponchotitlan
This script lints a YAML file to determine if it is properly formatted.
"""

import yaml
import sys

def lint_yaml(yaml_file_path):
    """
    Lint a YAML file to determine if it is properly formatted.

    Args:
        yaml_file_path (str): The path to the YAML file to lint.

    Returns:
        bool: True if the YAML file is properly formatted, False otherwise.
    """
    try:
        with open(yaml_file_path, 'r') as yaml_file:
            yaml.safe_load(yaml_file)
        return True
    except yaml.YAMLError as e:
        print(f"YAML Error: {e}")
        return False

if __name__ == "__main__":
    """
    Command-line interface for linting a YAML file.

    Usage:
        python lint_yaml.py <path_to_yaml_file>

    Exits with status code 0 if the YAML file is valid, otherwise exits with status code 1.
    """
    if len(sys.argv) != 2:
        print("Usage: python lint_yaml.py <path_to_yaml_file>")
        sys.exit(1)
    
    yaml_file_path = sys.argv[1]
    is_valid = lint_yaml(yaml_file_path)
    
    if is_valid:
        print(f"The YAML file ({yaml_file_path}) is properly formatted.")
        sys.exit(0)
    else:
        print(f"The YAML file ({yaml_file_path}) is not properly formatted.")
        sys.exit(1)